FROM ubuntu:22.04

MAINTAINER Pierre Navaro <pierre.navaro@inria.fr>

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -yq apt-utils && apt-get install -yq \
    git cmake g++ gfortran liblapack-dev libsuitesparse-dev libopenblas-dev \
    doxygen graphviz ghostscript blitz++ libblitz-dev libgmp-dev libmpfr-dev \
    libxml2-dev

RUN git clone https://gitlab.com/libeigen/eigen.git /opt/Eigen
RUN git clone https://github.com/DrTimothyAldenDavis/SuiteSparse.git --branch v4.5.3 --single-branch  /opt/SuiteSparse && cd /opt/SuiteSparse && make
